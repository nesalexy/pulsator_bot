from src.data_base.view_mail_log import MailLogOper
from src.google_drive.gd_connect import GoogleDriveConnect
from src.google_drive.gd_helper import GoogleDriveHelper
from src.mail.constant_settings import TIME_OUT_FOR_GETTING_MAIL_SEC, FOLDER_PROCESSED, FOLDER_INBOX, FOLDER_FAILED, \
    FOLDER_CRASHED
from src.mail.constant_status import STEP_GET_MAILS_FROM_FOLDER
from src.mail.imap_connect import ImapMailConnect
from src.mail.mail_sender import MailToDeveloper
from src.mail.mail_worker import WebMailGetter, WebmailMailSearcher


class CheckWebmailFolders:

    def __init__(self):
        self.connect_imap = ImapMailConnect("clients@mx.bokoredo.pro", "clientsmxbokoredopro123")
        self.connect_imap.__enter__()
        self.webmail_getter = WebMailGetter(self.connect_imap)
        self.log_mail = MailLogOper()
        self.webmail_work_folders = [FOLDER_INBOX, FOLDER_PROCESSED, FOLDER_FAILED, FOLDER_CRASHED]
        self.send_to_dev = MailToDeveloper()
        self.subj_mails_process_fold = None

    def check_webmail(self):
        for webmail_folder in self.webmail_work_folders:
            subjects_mails_db = self.get_all_processing_mail_db()
            webmail_mail = self.get_all_webmail_mail_from_folder(STEP_GET_MAILS_FROM_FOLDER + webmail_folder, 10, webmail_folder=webmail_folder)
            if webmail_mail:
                self.search_mails_in_webmail(webmail_mail, subjects_mails_db, webmail_folder)

    def get_all_processing_mail_db(self):
        subjects_mails_db = []
        processing_mails_db = self.log_mail.get_all_in_process_mails()
        for item_mail in processing_mails_db:
            subjects_mails_db.append(item_mail.theme_mess)
        return subjects_mails_db

    def get_all_webmail_mail_from_folder(self, step_descr, tomeout, webmail_folder):
        return self.webmail_getter.get_all_mails_from_folder(step_descr,
                                                             tomeout,
                                                             webmail_folder)

    def search_mails_in_webmail(self, webmail_mails, subject_mails, webmail_folder):
        if subject_mails:
            webmail_searcher = WebmailMailSearcher(webmail_mails, self.connect_imap, subject_mails)
            founded_webmail_mail_subjects = webmail_searcher.start_search()
            if founded_webmail_mail_subjects:
                for webmail_mail_subject in founded_webmail_mail_subjects:
                    mail_db = self.log_mail.get_mail_by_theme(webmail_mail_subject)
                    self.check_webmail_folder(mail_db, webmail_folder=webmail_folder)

    def check_webmail_folder(self, mail_db, webmail_folder):
        theme_mail = mail_db.theme_mess

        if webmail_folder == FOLDER_INBOX:
            self.log_mail.change_curr_folder(mail_db, FOLDER_INBOX)
            # send warning message
            mail_msg = 'Warning! Mail {} is still in {}'.format(theme_mail, FOLDER_INBOX)
            print(mail_msg)
            self.send_to_dev.generation_body_mail(mail_msg)

        elif webmail_folder == FOLDER_PROCESSED:
            self.log_mail.change_curr_folder(mail_db, FOLDER_PROCESSED)
            # check google drive
            print(theme_mail + " this mail in " + FOLDER_PROCESSED)
            self.subj_mails_process_fold.appened(theme_mail)

        elif webmail_folder == FOLDER_FAILED:
            self.log_mail.change_curr_folder(mail_db, FOLDER_FAILED)
            # send warning message
            mail_msg = 'Mail {} not correct processed it is in folder {}'.format(theme_mail, FOLDER_FAILED)
            print(mail_msg)
            self.send_to_dev.generation_body_mail(mail_msg)

        elif webmail_folder == FOLDER_CRASHED:
            self.log_mail.change_curr_folder(mail_db, FOLDER_CRASHED)
            # send warning message
            mail_msg = 'Mail {} not correct processed it is in folder {}'.format(theme_mail, FOLDER_CRASHED)
            print(mail_msg)
            self.send_to_dev.generation_body_mail(mail_msg)

        else:
            mail_msg = 'Mail {} not correct processed it is in folder '.format(theme_mail)
            print(mail_msg)
            self.send_to_dev.generation_body_mail(mail_msg)


class GoogleDriveChecker:

    def __init__(self, subjects_mails):
        self.gd_connect = GoogleDriveConnect()
        self.gd_helper = GoogleDriveHelper(self.gd_connect)
        self.subjects_mails = subjects_mails

    


t = CheckWebmailFolders()
t.check_webmail()



