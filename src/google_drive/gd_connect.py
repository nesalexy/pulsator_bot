from pydrive.auth import GoogleAuth, AuthenticationError, RefreshError
from pydrive.drive import GoogleDrive


class GoogleDriveConnect:

    g_drive = None
    drive_instance = None
    gauth = None

    def __init__(self):
        self.g_drive = GoogleDrive(self.gauth)

    def __new__(cls, *args, **kwargs):
        if cls.drive_instance is None:
            cls.gauth = GoogleAuth()
            try:
                cls.gauth.CommandLineAuth()
            except (AuthenticationError, RefreshError) as e:
                print(e)

            cls.drive_instance = super(GoogleDriveConnect, cls).__new__(cls)
        return cls.drive_instance