class GoogleDriveHelper:

    def __init__(self, g_drive):
        self.g_drive = g_drive

    def get_folder_by_name(self, title):
        folders = self.g_drive.ListFile({'q':  "title contains '%s' "
                                            "and mimeType='application/vnd.google-apps.folder'" % title,
                                            'maxResults': 1}).GetList()
        if folders:
            return folders[0]
        else:
            return None

    def upload_attachmnet(self, attachment_name, path, folder):
        gd_file = self.g_drive.CreateFile({'title': attachment_name,
                                                  "parents": [
                                                      {"kind": "drive#fileLink", "id": folder['id']}]})
        gd_file.SetContentFile(path)
        gd_file.Upload()

        return gd_file

    def get_files_from_folder(self, folder_id):
        return self.g_drive.ListFile({'q': "'{}' in parents and "
                                           "trashed=false".format(folder_id)}).GetList()

    def delete_file(self, file):
        file.Delete()