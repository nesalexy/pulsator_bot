import functools
import signal


def timeout_sec_func(timeout_seconds):
    def timeout_method(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, handler)
            signal.alarm(timeout_seconds)

            return func(*args, **kwargs)

        def handler(signum, frame):
            raise Exception("Timeout, method: " + str(func.__name__))

        def loop_forever(self):
            import time
            while 1:
                print("sec")
                time.sleep(1)

        return wrapper

    return timeout_method


