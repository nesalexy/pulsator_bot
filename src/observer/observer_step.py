import functools

from src.data_base.view_general_log import GeneralLogOper


class LoggerStatusDB:

    def __init__(self):
        print("init db")
        self.gen_log = GeneralLogOper()

    def set_general_log_status(self, type, status, exception=None):
        self.gen_log.add_inf(type, status, exception)
        print("db set status " + status + " type " + type)


class StepStatusManager:
    logger = LoggerStatusDB()

    @classmethod
    def general_logger_steps(cls, type):
        def logger_steps(func):
            @functools.wraps(func)
            def wrapper(*args, **kwargs):
                try:
                    f = func(*args, **kwargs)
                    cls.logger.set_general_log_status(type, "SUCCESS")
                    return f
                except BaseException as e:
                    cls.logger.set_general_log_status(type, "CRUSH", str(e))

            return wrapper

        return logger_steps

    @classmethod
    def insurance(cls, type):
        def logger_steps(func):
            @functools.wraps(func)
            def wrapper(*args, **kwargs):
                try:
                    return func(*args, **kwargs)
                except BaseException as e:
                    text_exception = "CRUSH: " + type + str(e)
                    cls.logger.set_general_log_status(type, "CRUSH", str(e))
                    raise Exception(text_exception)

            return wrapper

        return logger_steps