import smtplib
from email.header import Header
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate, formataddr
from time import sleep

from src.mail import constant_settings
from src.mail.constant_settings import TIME_OUT_GENERATION_WEBMAIL_MAIL
from src.mail.constant_status import STEP_SEND_EMAIL_WEBMAIL, STEP_GEN_ATTACHMENT_FROM_WEBMAIL
from src.mail.mail_helper import send_mail_by_smtp, generation_head_mail
from src.observer.observer_step import StepStatusManager
from src.observer.observer_time_method import timeout_sec_func


class MailToWebmail:

    def __init__(self, size_of_attachment, subject):
        self.size_of_attachment = size_of_attachment
        self.subject = subject

    @StepStatusManager.general_logger_steps(STEP_SEND_EMAIL_WEBMAIL)
    @timeout_sec_func(TIME_OUT_GENERATION_WEBMAIL_MAIL)
    def send_generate_mail(self):
        msg = MIMEMultipart()
        msg = generation_head_mail(msg,
                                   constant_settings.SEND_MAIL_SUBJECT,
                                   formataddr((str(Header(constant_settings.SEND_MAIL_FROM, 'utf-8')),
                                               constant_settings.SEND_MAIL_FROM)),
                                   constant_settings.SEND_MAIL_TO)
        msg = self.generation_multipart_mail(msg, ['media/test.jpg'])
        send_mail_by_smtp(msg)

    @StepStatusManager.general_logger_steps(STEP_GEN_ATTACHMENT_FROM_WEBMAIL)
    def generation_multipart_mail(self, msg, attachments):
        for file in attachments:
            part = MIMEApplication(open(file, 'rb').read())
            part.add_header('Content-Disposition', 'attachment', filename=file)
            msg.attach(part)

        return msg


class MailToDeveloper:

    def __init__(self):
        self.msg = MIMEMultipart()

    @timeout_sec_func(TIME_OUT_GENERATION_WEBMAIL_MAIL)
    def send_mail_to_dev(self, text_body):
        self.msg = generation_head_mail(self.msg,
                                   constant_settings.SEND_MAIL_FROM,
                                   formataddr((str(Header(constant_settings.SEND_MAIL_CRASH_THEME, 'utf-8')),
                                               constant_settings.SEND_MAIL_CRASH_THEME)),
                                   constant_settings.SEND_MAIL_CRASH_TO_DEVELOPERS)
        self.generation_body_mail(text_body)
        send_mail_by_smtp(self.msg)

    def generation_body_mail(self, text_body):
        body = str(text_body)
        body = MIMEText(body)
        self.msg.attach(body)
