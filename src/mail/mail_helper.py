import email
import smtplib
import time
from datetime import datetime, timedelta
from email.utils import formatdate

from src.mail import constant_settings
from src.mail.constant_settings import MAIL_QUERY_DAYS, TIME_OUT_SEND_MAIL
from src.mail.constant_status import STEP_GET_DATE_FOR_QUERY_MAIL
from src.observer.observer_step import StepStatusManager
from src.observer.observer_time_method import timeout_sec_func


@StepStatusManager.insurance(STEP_GET_DATE_FOR_QUERY_MAIL)
def generation_query_date_mail():
    formatter_query_date = "%d-%b-%Y"
    query_date = '(SINCE "{}" BEFORE "{}")'

    calc_date_after = datetime.today() + timedelta(days=1)
    date_after = calc_date_after.strftime(formatter_query_date)

    calc_date_before = datetime.today() - timedelta(days=MAIL_QUERY_DAYS)
    date_before = calc_date_before.strftime(formatter_query_date)

    query = query_date.format(date_before, date_after)
    return query


def get_mail_subject(mail):
    mail_subject = mail['Subject']
    if mail_subject:
        return mail_subject


def get_mail_from(mail):
    mail_from = mail['From']
    if mail_from:
        return mail_from


def get_current_timestamp():
    ts = time.time()
    return str(ts)


def get_mail_body(data):
    email_body = data[0][1].decode('utf8', 'ignore').encode("ascii", errors="ignore")
    mail = email.message_from_bytes(email_body)
    return mail


def send_mail_by_smtp(msg):
    server = smtplib.SMTP(constant_settings.SEND_MAIL_HOST, constant_settings.SEND_MAIL_PORT, timeout=10)
    server.starttls()
    server.login(constant_settings.SEND_MAIL_USER_EMAIL, constant_settings.SEND_MAIL_USER_PASSWORD)
    text = msg.as_string()
    server.sendmail(constant_settings.SEND_MAIL_USER_EMAIL,
                    constant_settings.SEND_MAIL_TO,
                    text)


def generation_head_mail(msg,
                             mail_subject,
                             mail_from,
                             mail_to):
        msg['Subject'] = mail_subject
        msg['From'] = mail_from
        msg['To'] = mail_to
        msg['Date'] = formatdate(localtime=True)

        return msg