import email

from src.mail.constant_settings import TIME_OUT_SEARCH_MAIL_THEMES
from src.mail.constant_status import STEP_START_SEARCH_MAIL_THEMES, STEP_CHECK_IS_MY_MAIL, STEP_SET_CURRENT_FOLDER_MAIL
from src.mail.mail_helper import generation_query_date_mail, get_mail_body, get_mail_subject
from src.observer.observer_step import StepStatusManager
from src.observer.observer_time_method import timeout_sec_func


class WebMailGetter:

    def __init__(self, connect_imap):
        self.connect_imap = connect_imap

    def get_all_mails_from_folder(self, log_step, timeout, folder):

        @StepStatusManager.general_logger_steps(log_step)
        @timeout_sec_func(timeout)
        def __get_all_mails_from_folder(folder):
            self.connect_imap.imap.select(folder)
            search_params = generation_query_date_mail()
            if search_params:
                status, responce = self.connect_imap.imap.search(None, search_params)

                if status == 'OK':
                    items = responce[0].split()
                    if items:
                        return items

        return __get_all_mails_from_folder(folder)


class WebmailMailSearcher:

    def __init__(self, mails, connect_imap, subjects_search_mails):
        self.mails = mails
        self.connect_imap = connect_imap
        self.subjects_search_mails = subjects_search_mails
        print(self.subjects_search_mails)

    @StepStatusManager.general_logger_steps(STEP_START_SEARCH_MAIL_THEMES)
    @timeout_sec_func(TIME_OUT_SEARCH_MAIL_THEMES)
    def start_search(self):
        founded_mails_subj = []
        for email_id in self.mails:
            status, data = self.connect_imap.imap.fetch(email_id, "(RFC822)")
            if status == 'OK':
                mail = get_mail_body(data)
                subject = get_mail_subject(mail)
                print(subject)
                if mail:
                    if self.__is_my_email(subject):
                        founded_mails_subj.append(subject)

        return founded_mails_subj

    @StepStatusManager.insurance(STEP_CHECK_IS_MY_MAIL)
    def __is_my_email(self, subject):
        if subject:
            return subject in self.subjects_search_mails
        else:
            return False
