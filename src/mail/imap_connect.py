import imaplib

from src.mail.constant_settings import TIME_OUT_CONNECT_IMAP_SEC, IMAP_USE_SSL, IMAP_SERVER, IMAP_PORT, \
    TIME_OUT_IMAP_LOGIN_SEC, TIME_OUT_IMAP_LOGOUT_SEC
from src.mail.constant_status import STEP_CONNECT_IMAP, STEP_LOG_IN_MAIL, STEP_LOG_OUT_MAIL
from src.observer.observer_step import StepStatusManager
from src.observer.observer_time_method import timeout_sec_func


class ImapMailConnect:

    @StepStatusManager.general_logger_steps(STEP_CONNECT_IMAP)
    @timeout_sec_func(TIME_OUT_CONNECT_IMAP_SEC)
    def __init__(self, user, password):
        """
        connect IMAP4

        Keyword arguments:
        user -- email user
        password -- user password
        """
        self.user = user
        self.password = password
        self.imap = None
        if IMAP_USE_SSL:
            self.imap = imaplib.IMAP4_SSL(IMAP_SERVER, IMAP_PORT)
        else:
            self.imap = imaplib.IMAP4(IMAP_SERVER, IMAP_PORT)

    @StepStatusManager.general_logger_steps(STEP_LOG_IN_MAIL)
    @timeout_sec_func(TIME_OUT_IMAP_LOGIN_SEC)
    def __enter__(self):
        """
        after create object ImapMailConnect need call __enter__
        """
        if self.imap:
            self.imap.login(self.user, self.password)
        return self

    @StepStatusManager.general_logger_steps(STEP_LOG_OUT_MAIL)
    @timeout_sec_func(TIME_OUT_IMAP_LOGOUT_SEC)
    def __exit__(self):
        """
        when you are finished all operations with IMAP4, need call __exit__
        """
        if self.imap:
            self.imap.close()
            self.imap.logout()