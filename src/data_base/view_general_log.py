from src.data_base.connector import ConnectorDB
from src.data_base.models import GeneralLog


class GeneralLogOper:

    @staticmethod
    def add_inf(type, status, exception=None):
        with ConnectorDB():
            GeneralLog.create(type=type,
                              status=status,
                              exception=exception)