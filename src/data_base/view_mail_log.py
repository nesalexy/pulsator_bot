from src.data_base.connector import ConnectorDB
from src.data_base.constants import FIELD_IN_PROCESSED
from src.data_base.models import MailLog
from src.mail.mail_helper import get_current_timestamp


class MailLogOper:

    @staticmethod
    def add_mail(theme_mess, time_send, curr_folder_mail, status, time_check):

        with ConnectorDB():
            MailLog.create(theme_mess=theme_mess,
                                      time_send=time_send,
                                      time_check=time_check,
                                      curr_folder_mail=curr_folder_mail,
                                      status=status)

    @staticmethod
    def get_mail_by_theme(theme_mess):

        with ConnectorDB():
            mail = MailLog.select().where(MailLog.theme_mess == theme_mess).get()
            return mail

    @staticmethod
    def get_all_in_process_mails():

        with ConnectorDB():
            mails = MailLog.select().where(MailLog.status == FIELD_IN_PROCESSED)
            return mails

    @staticmethod
    def change_curr_folder(item_table_mail, current_folder):

        with ConnectorDB():
            if item_table_mail.curr_folder_mail != current_folder:
                item_table_mail.curr_folder_mail = current_folder
                item_table_mail.save()

    @staticmethod
    def change_curr_status(item_table_mail, current_status):

        with ConnectorDB():
            item_table_mail.status = current_status
            item_table_mail.save()
