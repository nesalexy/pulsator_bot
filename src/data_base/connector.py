from src.data_base.models import before_request_handler, after_request_handler


class ConnectorDB:

    def __enter__(self):
        before_request_handler()

    def __exit__(self, exc_type, exc_val, exc_tb):
        after_request_handler()
