import datetime

import peewee

from src.data_base import constants

database = peewee.SqliteDatabase(constants.DB_NAME)


class MailLog(peewee.Model):
    """
    ORM model of general log table.
    This table contains information about each mail who was send
    """
    theme_mess = peewee.CharField(null=False)
    time_send = peewee.DateField(null=False)
    curr_folder_mail = peewee.CharField(null=True)
    status = peewee.CharField(null=True)
    created_at = peewee.DateTimeField(default=datetime.datetime.now())
    updated_at = peewee.DateTimeField(default=datetime.datetime.now())

    class Meta:
        database = database


class StatusLog(peewee.Model):
    """
    ORM model of system log table
    This table contains information about each step for check mail
    """
    mail_log = peewee.ForeignKeyField(MailLog)
    type = peewee.CharField()
    status = peewee.CharField()
    exception = peewee.CharField(null=True)

    class Meta:
        database = database


class AttachmentDetail(peewee.Model):
    """
    ORM model of attachment detail table
    This table contains information about sended attachment
    """
    mail_log = peewee.ForeignKeyField(MailLog)
    origin_name = peewee.CharField()
    hash_summ = peewee.CharField()
    status = peewee.CharField()

    class Meta:
        database = database


class GeneralLog(peewee.Model):
    """
    ORM model of attachment detail table
    This table contains information about general steps script
    """
    type = peewee.CharField()
    status = peewee.CharField()
    exception = peewee.CharField(null=True)

    class Meta:
        database = database


def before_request_handler():
    create_tables()

    if database.is_closed():
        database.connect()


def after_request_handler():
    database.close()


def create_tables():
    with database:
        database.create_tables([MailLog, StatusLog, AttachmentDetail, GeneralLog])
