import hashlib


class FileHelper:

    def __init__(self):
        pass

    def get_hash_from_file(self, file_path):
        return hashlib.md5(open(file_path, 'rb').read()).hexdigest()